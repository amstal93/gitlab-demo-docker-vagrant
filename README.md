# Description

## Vagrant

Vagrant provides a basic Ubuntu environment with Docker and Docker Compose pre-installed
Thanks to:
* [leighmcculloch/vagrant-docker-compose](https://github.com/leighmcculloch/vagrant-docker-compose)
* [Vagrant Docker Provisioner](https://www.vagrantup.com/docs/provisioning/docker)

Specs:

* IP: 192.168.50.4
* Hostname: gitlab.example.com
* 4 GB memory and 4 core

Only run:

```shell
vagrant up
```

## Install GitLab on a container

### Using Docker Engine

```shell
vagrant@ubuntu-bionic$ sudo docker run --detach \
  --hostname gitlab.example.com \
  --publish 443:443 \
  --publish 80:80 \
  --publish 2222:22 \
  --name gitlab \
  --restart always \
  --volume /srv/gitlab/config:/etc/gitlab \
  --volume /srv/gitlab/logs:/var/log/gitlab \
  --volume /srv/gitlab/data:/var/opt/gitlab \
  gitlab/gitlab-ce:latest
```

### Using Docker Compose (+install and manually register GitLab Runner)

Notes on different docker compose files:

* `./nginx/docker-compose.yml`  --> to run a simple NginX server (can be used to check HTTP connectivity). 

* `./gitlab/docker-compose.yml`  --> to run GitLab CE only.

* `./gitlab-and-runner/docker-compose.yml`  --> to run GitLab CE and GitLab Runner in different containers *THIS WILL BE USED FURTHER!*

#### Install & Configure

```shell
vagrant@ubuntu-bionic$ cd /vagrant/gitlab-and-runner
vagrant@ubuntu-bionic$ docker-compose up --detach
```

After a few minutes (or longer if *gitlab-ce* image was not installed) GitLab server can be reached from the host computer using one of the following:

* <http://192.168.50.4:80>

* <http://127.0.0.1:8888>

```shell
vagrant@ubuntu-bionic$ docker logs -f gitlab-and-runner_gitlab-runner_1
```

Following error will be received from *gitlab-runner* container since the runner has not registered the runner yet:

<span style="color:red">ERROR: Failed to load config stat /etc/gitlab-runner/config.toml: no such file or directory  builds=0</span>

After receiving the Registration Token for the GitLab runner following command can be executed:

```shell
vagrant@ubuntu-bionic$ docker exec -it gitlab-and-runner_gitlab-runner_1 /bin/bash
```

```shell
bash# gitlab-runner register \
--non-interactive \
--url "http://gitlab.example.com" \
--registration-token "<PROJECT_REGISTRATION_TOKEN>" \
--executor "docker" \
--docker-image "alpine:latest" \
--description "docker-runner" \
--tag-list "demo-runner, gitlab-runner" \
--run-untagged="true" 
```

***Note:**
A lot of steps & configurations could be improved here... Still this project can be used to play around with a simple GitLab environment.*
